// Authors: Josh Bourquin

// Style scripts
import "@blueprintjs/core/lib/css/blueprint.css"
import "@blueprintjs/icons/lib/css/blueprint-icons.css"
import "./App.css"

// React components
import React, { Component } from 'react'
import SatTracker from './components/SatTracker'

const satellites = [
    {
      name: 'Lemur 2 Jin-Luen',
      tle: [
        '1 43182U 18014C   18211.69527835  .00000790  00000-0  75686-4 0  9992',
        '2 43182  97.7164 119.2968 0014826  15.7631 344.4048 14.95888956 26811'
      ]
    },
    {
      name: 'Lemur 2 Marshall',
      tle: [
        '1 43165U 18010C   18211.83613230 +.00001253 +00000-0 +58278-4 0  9993',
        '2 43165 082.9200 320.8279 0030556 297.1457 062.6663 15.18560549028943'
      ]
    },
    {
      name: 'Lemur 2 KungFoo',
      tle: [
        '1 42774U 17036K   18211.72910168  .00000929  00000-0  44552-4 0  9992',
        '2 42774  97.3984 270.4584 0012627 272.8731  87.1060 15.21326746 61112'
      ]
    },
    {
      name: 'Lemur 2 Zachary',
      tle: [
        '1 42845U 17042W   18211.65986658  .00000401  00000-0  44788-4 0  9993',
        '2 42845  97.5882 112.7303 0014655  58.5555 301.7095 14.92023371 56845'
      ]
    }
]

// Main application
class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      datetime: new Date()
    }
    this.animate = this.animate.bind(this)
  }

  componentDidMount() {
    setInterval(this.animate, 1000)
  }

  animate() {
    this.setState({datetime: new Date()})
  }

  render() {
    return (
      <div className="app">
        <SatTracker datetime={this.state.datetime} satellites={satellites}/>
      </div>
    );
  }
}

export default App;
